#Bloodgroves
1012 = "Ilushinaboli" #Blood Moon forest
1119 = "Ishepshain" #Blood Feeder River
1120 = "Ugshamaboli" #Winter Forest
1121 = "Erit'hiagola" #Frozen Weeping
1123 = "Hdamutcha" #Far Mountain
1995 = "Hdakipsasp" #Beyond our Field
2459 = "Aboliuhri" #Border Forest 
2538 = "Ishepshaboli" #Blood Feeder Forest

#Dalaire
1200 = "Monoebl" #Temple of Mono
1216 = "Khezken" #Fur Place
1217 = "Nadhtarak" #Dark Spear
1218 = "Qelnardir" #High Camp
1224 = "Kanchos" #River Woods
1236 = "Akewrdir" #Ichor Camp
1237 = "Tugrkhir" #Bone Axe
1238 = "Kheilderin" #Blood Tree
1239 = "Kanderin" #Tree River
1240 = "Shwarkhir" #Wicked Crushers
1241 = "Erweradh" #Broken Path
2794 = "Appaiq" #Bird Island
2848 = "Radhcalrm" #Hill Path
2853 = "Kezrtaseth" #Battle Clearing

#Broken Isles
1746 = "Ardirken" #Place of the Camp